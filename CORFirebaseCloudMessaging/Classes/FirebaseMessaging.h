#import <UIKit/UIKit.h>
#import "Firebase.h"

@interface FirebaseMessaging : UIResponder<FIRMessagingDelegate>

@property(nonatomic, strong) UIWindow *window;
+ (FirebaseMessaging*)sharedManager;
- (void)fetchToken;
- (void)fetch;
- (void)pushInit;
@end
