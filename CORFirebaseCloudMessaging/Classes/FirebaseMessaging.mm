
#import "FirebaseMessaging.h"
#import <CORCommon/CORCommon.h>
#import <UserNotifications/UserNotifications.h>

@interface FirebaseMessaging () <UNUserNotificationCenterDelegate>
@end

@implementation FirebaseMessaging

NSString *kGCMMessageIDKey = @"gcm.message_id";
NSString *unityObject = @"BBFirebaseMessagingService";
NSString *jsonStringUserData = nil;
UIApplication *app;

+ (FirebaseMessaging*)sharedManager
{
    static FirebaseMessaging *sharedSingleton;
    
    if( !sharedSingleton )
    {
        sharedSingleton = [[FirebaseMessaging alloc] init];
    }
    
    return sharedSingleton;
}

- (void)pushInit {
    NSLog(@"FirebaseMessaging :: pushInit");
    
    if ([UNUserNotificationCenter class] != nil) {
      [UNUserNotificationCenter currentNotificationCenter].delegate = self;
      UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
          UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
      [[UNUserNotificationCenter currentNotificationCenter]
          requestAuthorizationWithOptions:authOptions
          completionHandler:^(BOOL granted, NSError * _Nullable error) {
          NSLog(@"FirebaseMessaging :: pushInit :: user notification registered: %d", granted);
          }];
    }
}

- (void)fetchToken {
    NSLog(@"FirebaseMessaging :: fetchToken");
    [FIRMessaging messaging].delegate = self;
    
    [[FIRMessaging messaging] tokenWithCompletion:^(NSString *token, NSError *error) {
      if (error != nil) {
          NSLog(@"FirebaseMessaging :: fetchToken :: Error: %@", error);
      } else {
          NSLog(@"FirebaseMessaging :: fetchToken :: Token: %@", token);
          UnitySendMessage( unityObject.UTF8String, "OnTokenFetchSucceeded",
                           token.UTF8String);
      }
    }];
}

- (void)fetch {
    NSLog(@"FirebaseMessaging :: fetch");
    if( jsonStringUserData != nil ) {
    
        UnitySendMessage( unityObject.UTF8String, "OnMessageReceived",
                     jsonStringUserData.UTF8String);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"FirebaseMessaging :: didReceiveRemoteNotification :: Message: %@", userInfo[kGCMMessageIDKey]);
    }

    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    NSLog(@"FirebaseMessaging :: didReceiveRemoteNotification :: Message: %@", userInfo);
    jsonStringUserData = ([self DataTOjsonString:userInfo]);
    UnitySendMessage( unityObject.UTF8String, "OnMessageReceived",
                     jsonStringUserData.UTF8String);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  
    if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"FirebaseMessaging :: didReceiveRemoteNotification :: Message: %@", userInfo[kGCMMessageIDKey]);
    }

    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    NSLog(@"FirebaseMessaging :: didReceiveRemoteNotification :: Message: %@", userInfo);
    jsonStringUserData = ([self DataTOjsonString:userInfo]);
    UnitySendMessage( unityObject.UTF8String, "OnMessageReceived",
                     jsonStringUserData.UTF8String);
    
    if (userInfo[@"CONFIG_STATE"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CONFIG_STALE"];
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"FirebaseMessaging :: userNotificationCenter :: Message: %@", userInfo[kGCMMessageIDKey]);
    }

    jsonStringUserData = ([self DataTOjsonString:userInfo]);
    UnitySendMessage( unityObject.UTF8String, "OnMessageReceived",
                     jsonStringUserData.UTF8String);
    
    if (userInfo[@"CONFIG_STATE"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CONFIG_STALE"];
    }
    
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"FirebaseMessaging :: userNotificationCenter :: Message: %@", userInfo[kGCMMessageIDKey]);
    }
    NSLog(@"FirebaseMessaging :: userNotificationCenter :: UserInfo: %@", userInfo);
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    
    jsonStringUserData = ([self DataTOjsonString:userInfo]);
    UnitySendMessage( unityObject.UTF8String, "OnMessageReceived",
                     jsonStringUserData.UTF8String);
    
    completionHandler();
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FirebaseMessaging :: didReceiveRegistrationToken :: Registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
    [[FIRMessaging messaging] subscribeToTopic:@"PUSH_RC" completion:^(NSError * _Nullable error) {
         NSLog(@"FirebaseMessaging :: didReceiveRegistrationToken :: Subscribed to PUSH_RC topic");
     }];
}

-(NSString*)DataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"FirebaseMessaging :: DataTOjsonString :: Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}


@end


extern "C" {


const char* GetObjectName() {
    return MakeStringCopy(unityObject);
}
const char* GetNotifications() {
    return MakeStringCopy(jsonStringUserData);
}
void ClearNotifications()
{
    jsonStringUserData = nil;
}
void FetchToken()
{
    [[FirebaseMessaging sharedManager] fetchToken];
}
void Fetch()
{
    [[FirebaseMessaging sharedManager] fetch];
}

void FirebaseRegisterPush()
{
    [[FirebaseMessaging sharedManager] pushInit];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

}
