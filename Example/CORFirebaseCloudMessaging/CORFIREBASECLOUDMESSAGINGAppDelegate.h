//
//  CORFIREBASECLOUDMESSAGINGAppDelegate.h
//  CORFirebaseCloudMessaging
//
//  Created by Damian Grzybinski on 12/02/2021.
//  Copyright (c) 2021 Damian Grzybinski. All rights reserved.
//

@import UIKit;

@interface CORFIREBASECLOUDMESSAGINGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
