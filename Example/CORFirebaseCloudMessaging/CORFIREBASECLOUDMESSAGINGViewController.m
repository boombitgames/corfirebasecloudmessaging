//
//  CORFIREBASECLOUDMESSAGINGViewController.m
//  CORFirebaseCloudMessaging
//
//  Created by Damian Grzybinski on 12/02/2021.
//  Copyright (c) 2021 Damian Grzybinski. All rights reserved.
//

#import "CORFIREBASECLOUDMESSAGINGViewController.h"

@interface CORFIREBASECLOUDMESSAGINGViewController ()

@end

@implementation CORFIREBASECLOUDMESSAGINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
